<?php

namespace App\Http\Controllers;

use App\Models\BadanUsaha;
use App\Http\Requests\StoreBadanUsahaRequest;
use App\Http\Requests\UpdateBadanUsahaRequest;

class BadanUsahaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreBadanUsahaRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBadanUsahaRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BadanUsaha  $badanUsaha
     * @return \Illuminate\Http\Response
     */
    public function show(BadanUsaha $badanUsaha)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BadanUsaha  $badanUsaha
     * @return \Illuminate\Http\Response
     */
    public function edit(BadanUsaha $badanUsaha)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateBadanUsahaRequest  $request
     * @param  \App\Models\BadanUsaha  $badanUsaha
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBadanUsahaRequest $request, BadanUsaha $badanUsaha)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BadanUsaha  $badanUsaha
     * @return \Illuminate\Http\Response
     */
    public function destroy(BadanUsaha $badanUsaha)
    {
        //
    }
}
