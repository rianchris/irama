 <h5 class="card-header">Perolehan Informasi:</h5>
 <div class="card-body">
     <div class="row">
         <div class="col-md mb-md-0 mb-2">
             <div class="form-check custom-option custom-option-icon">
                 <label class="form-check-label custom-option-content" for="dokumen">
                     <span class="custom-option-body">
                         <i class='bx bxs-file-doc'></i>
                         <span class="custom-option-title"> Dokumen </span>
                         <small> Cake sugar plum fruitcake I love sweet roll jelly-o. </small>
                     </span>
                     <input class="form-check-input" type="checkbox" value="" id="dokumen" />
                 </label>
             </div>
         </div>
         <div class="col-md mb-md-0 mb-2">
             <div class="form-check custom-option custom-option-icon">
                 <label class="form-check-label custom-option-content" for="wawancara">
                     <span class="custom-option-body">
                         <i class='bx bxs-microphone'></i>
                         <span class="custom-option-title"> Wawancara </span>
                         <small> Cake sugar plum fruitcake I love sweet roll jelly-o. </small>
                     </span>
                     <input class="form-check-input" type="checkbox" value="" id="wawancara" />
                 </label>
             </div>
         </div>
         <div class="col-md">
             <div class="form-check custom-option custom-option-icon">
                 <label class="form-check-label custom-option-content" for="kebijakan">
                     <span class="custom-option-body">
                         <i class='bx bxs-flag-alt'></i>
                         <span class="custom-option-title"> Kebijakan </span>
                         <small> Cake sugar plum fruitcake I love sweet roll jelly-o. </small>
                     </span>
                     <input class="form-check-input" type="checkbox" value="" id="kebijakan" />
                 </label>
             </div>
         </div>
         <div class="col-md">
             <div class="form-check custom-option custom-option-icon">
                 <label class="form-check-label custom-option-content" for="observasi">
                     <span class="custom-option-body">
                         <i class='bx bx-search-alt-2'></i>
                         <span class="custom-option-title"> Observasi </span>
                         <small> Cake sugar plum fruitcake I love sweet roll jelly-o. </small>
                     </span>
                     <input class="form-check-input" type="checkbox" value="" id="observasi" />
                 </label>
             </div>
         </div>
     </div>
 </div>
