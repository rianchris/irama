<footer class="content-footer footer bg-footer-theme">
    <div class="container-xxl d-flex flex-wrap justify-content-between py-2 flex-md-row flex-column">
        <div class="mb-2 mb-md-0">
            ©
            <script>
                document.write(new Date().getFullYear());
            </script>
            , made with ❤️ by
            <a target="_blank" class="footer-link fw-bolder">TIM Pengembangan MR</a>
        </div>
        <div>
            <a href="https://bpkp.go.id" target="_blank" class="footer-link me-4">BPKP</a>
            <a href="https://dan.bpkp.go.id/portal" class="footer-link me-4" target="_blank">Deputi Bidang Akuntan Negara</a>
        </div>
    </div>
</footer>
